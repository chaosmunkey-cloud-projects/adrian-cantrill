# Adrian Cantrill Labs

The projects in this section of the repo follow Adrian Cantrill's [Learn IO Labs](https://github.com/acantril/learn-cantrill-io-labs).

Throughtout the mini projects, Adrian uses [CloudFormation](https://aws.amazon.com/cloudformation/) for deploying the initial infrastructure followed by steps to get you to the final outcome. I however have decided to go down a different route of deploying the various parts using [Terraform](https://www.terraform.io/) from *HashiCorp*.

Each stage will be its own terraform project building upon previous stages.
